package com.inxedu.os.edu.dao.webpagetemplate;

import com.inxedu.os.common.entity.PageEntity;
import com.inxedu.os.edu.entity.webpagetemplate.WebpageTemplate;

import java.util.List;

/**
 * @author www.inxedu.com
 * @description 页面和模板中间表 WebpageTemplateDao接口
 */
public interface WebpageTemplateDao{
	/**
     * 添加页面和模板中间表
     */
    Long addWebpageTemplate(WebpageTemplate webpageTemplate);
    
    /**
     * 删除页面和模板中间表
     * @param id
     */
    void delWebpageTemplateById(Long id);
    
    /**
     * 修改页面和模板中间表
     * @param webpageTemplate
     */
    void updateWebpageTemplate(WebpageTemplate webpageTemplate);
    
    /**
     * 通过id，查询页面和模板中间表
     * @param id
     * @return
     */
    WebpageTemplate getWebpageTemplateById(Long id);
    
    /**
     * 分页查询页面和模板中间表列表
     * @param webpageTemplate 查询条件
     * @param page 分页条件
     * @return List<WebpageTemplate>
     */
    List<WebpageTemplate> queryWebpageTemplateListPage(WebpageTemplate webpageTemplate, PageEntity page);
    
    /**
     * 条件查询页面和模板中间表列表
     * @param webpageTemplate 查询条件
     * @return List<WebpageTemplate>
     */
    List<WebpageTemplate> queryWebpageTemplateList(WebpageTemplate webpageTemplate);

    /**
     * 根据页面id 删除页面和模板中间表 的记录
     * @param webpageId
     */
    void delWebpageTemplateByWebpageId(Long webpageId);

    /**
     * 根据 条件 删除页面和模板中间表
     * @param webpageTemplate
     */
    void delWebpageTemplate(WebpageTemplate webpageTemplate);
}