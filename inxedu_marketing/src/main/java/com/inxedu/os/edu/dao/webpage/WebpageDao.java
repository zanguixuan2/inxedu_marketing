package com.inxedu.os.edu.dao.webpage;

import com.inxedu.os.common.entity.PageEntity;
import com.inxedu.os.edu.entity.webpage.Webpage;

import java.util.List;

/**
 * @author www.inxedu.com
 * @description 页面 WebpageDao接口
 */
public interface WebpageDao{
	/**
     * 添加页面
     */
    public Long addWebpage(Webpage webpage);
    
    /**
     * 删除页面
     * @param id
     */
    public void delWebpageById(Long id);
    
    /**
     * 修改页面
     * @param webpage
     */
    public void updateWebpage(Webpage webpage);
    
    /**
     * 通过id，查询页面
     * @param id
     * @return
     */
    public Webpage getWebpageById(Long id);
    
    /**
     * 分页查询页面列表
     * @param webpage 查询条件
     * @param page 分页条件
     * @return List<Webpage>
     */
    public List<Webpage> queryWebpageListPage(Webpage webpage, PageEntity page);
    
    /**
     * 条件查询页面列表
     * @param webpage 查询条件
     * @return List<Webpage>
     */
    public List<Webpage> queryWebpageList(Webpage webpage);
}



