package com.inxedu.os.edu.dao.impl.webpage;

import com.inxedu.os.common.dao.GenericDaoImpl;
import com.inxedu.os.common.entity.PageEntity;
import com.inxedu.os.edu.dao.webpage.WebpageDao;
import com.inxedu.os.edu.entity.webpage.Webpage;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author www.inxedu.com
 * @description 页面 WebpageDao接口实现
 */
@Repository("webpageDao")
public class WebpageDaoImpl extends GenericDaoImpl implements WebpageDao{
	/**
     * 添加页面
     */
    public Long addWebpage(Webpage webpage){
    	this.insert("WebpageMapper.addWebpage", webpage);
		return webpage.getId();
    }
    
    /**
     * 删除页面
     * @param id
     */
    public void delWebpageById(Long id){
    	this.update("WebpageMapper.delWebpageById", id);
    }
    
    /**
     * 修改页面
     * @param webpage
     */
    public void updateWebpage(Webpage webpage){
    	this.update("WebpageMapper.updateWebpage", webpage);
    }
    
    /**
     * 通过id，查询页面
     * @param id
     * @return
     */
    public Webpage getWebpageById(Long id){
    	return this.selectOne("WebpageMapper.getWebpageById", id);
    }
    
    /**
     * 分页查询页面列表
     * @param webpage 查询条件
     * @param page 分页条件
     * @return List<Webpage>
     */
    public List<Webpage> queryWebpageListPage(Webpage webpage,PageEntity page){
    	return this.queryForListPage("WebpageMapper.queryWebpageListPage", webpage, page);
    }
    
    /**
     * 条件查询页面列表
     * @param webpage 查询条件
     * @return List<Webpage>
     */
    public List<Webpage> queryWebpageList(Webpage webpage){
    	return this.selectList("WebpageMapper.queryWebpageList", webpage);
    }
}



