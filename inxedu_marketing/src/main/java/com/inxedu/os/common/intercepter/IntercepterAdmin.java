package com.inxedu.os.common.intercepter;

import com.inxedu.os.common.util.SingletonLoginUtils;
import com.inxedu.os.edu.constants.enums.WebSiteProfileType;
import com.inxedu.os.edu.entity.system.SysUser;
import com.inxedu.os.edu.service.website.WebsiteProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 后台用户登录与权限拦截器
 *@author www.inxedu.com
 */
public class IntercepterAdmin extends HandlerInterceptorAdapter{

	@Autowired
	private WebsiteProfileService websiteProfileService;

	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		super.afterCompletion(request, response, handler, ex);
	}

	
	public void afterConcurrentHandlingStarted(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		super.afterConcurrentHandlingStarted(request, response, handler);
	}

	
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		super.postHandle(request, response, handler, modelAndView);
	}

	@SuppressWarnings("unchecked")
	
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		//获得网站配置、LOGO配置、网站统计代码、咨询链接
		Map<String,Object> websitemap=websiteProfileService.getWebsiteProfileByType(WebSiteProfileType.web.toString());
		request.setAttribute("websitemap",websitemap);
		//获取登录的用户
		SysUser sysUser = SingletonLoginUtils.getLoginSysUser(request);
		if(sysUser==null){
			response.sendRedirect("/admin");//跳转登录页面
			return false;
		}
		request.setAttribute("sysuser", sysUser);
		return true;
	}

}
