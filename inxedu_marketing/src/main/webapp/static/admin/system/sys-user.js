$(function(){
	$(".ui-dialog-titlebar-close,.closeBut").click(function(){
		closedData();
	});
});

/** 
 * 显示添加窗口 
 */
function showWin(){
	closedData();
	$("#createWin").show();
}

/**
 * 点击查找用户
 */
function searchUser(){
	$("#searchForm").submit();
}

/**
 * 禁用或启用后台用户
 * @param userId 用户ID
 * @param type 1启用 2禁用
 * @param em 
 */
function disableOrstart(userId,type,em){
	closedData();
	$.ajax({
		url:baselocation+'/admin/sysuser/disableOrstart/'+userId+'/'+type,
		type:'post',
		dataType:'json',
		success:function(result){
			if(result.success==true){
				var td = $(em).parent('samp').parent('td').parent('tr').children('td')[7];
				if(type==1){
					$(td).text('正常');
					$(em).parent('samp').html('<button onclick="disableOrstart('+userId+',2,this)" class="ui-state-default ui-corner-all" type="button">冻结</button>');
				}else if(type==2){
					$(td).text('冻结');
					$(em).parent('samp').html('<button onclick="disableOrstart('+userId+',1,this)" class="ui-state-default ui-corner-all" type="button">启用</button>');
				}
			}else{
				msgshow(result.message);
			}
		},
		error:function(error){
			msgshow("系统繁忙，请稍后再操作");
		}
	});
}

/**
 * 显示修改密码窗口
 */
function updatePwd(userId){
	closedData();
	$("#updatePwdWin").show();
	$("#updatePwdWin #sysUserId").val(userId);
}

/**
 * 提交修改密码
 */
function submitUpdatePwd(){
	var passArr = $("#updatePwdWin input:password");
	if(passArr[0].value==null  || passArr[1].value==null || $.trim(passArr[1].value)==''||$.trim(passArr[0].value)==''){
		msgshow("请输入新密码！");
		return false;
	}
	if(passArr[0].value!=passArr[1].value){
		msgshow('两次密码不一致！');
		return false;
	}
	var userId = $("#updatePwdWin #sysUserId").val()
	$.ajax({
		url:baselocation+'/admin/sysuser/updatepwd/'+userId,
		type:'post',
		dataType:'json',
		data:{'newPwd':passArr[0].value},
		success:function(result){
			if(result.success==true){
				closedData();
			}
			msgshow(result.message);
		},
		error:function(error){
			msgshow('系统繁忙，请稍后再操作！');
		}
	});
}

/**
 * 执行修改用户信息
 */
function updateSysUser(){
	var params = ''
	$("#updateSysUserForm input,#updateSysUserForm select").each(function(){
		params+=$(this).serialize()+"&";
    });
	$.ajax({
		url:baselocation+'/admin/sysuser/updateuser',
		type:'post',
		dataType:'json',
		data:params,
		success:function(result){
			if(result.success==true){
				closedData();
			}
			msgshow(result.message);
		},
		error:function(error){
			msgshow("系统繁忙，请稍后再操作！");
		}
	});
}

/**
 * 初始化修改用户
 * @param userId
 */
function initUser(userId){
	closedData();
	$.ajax({
		url:baselocation+'/admin/sysuser/initupdateuser/'+userId,
		type:'post',
		dataType:'json',
		success:function(result){
			if(result.success==true){
				var user = result.entity;
				$("#updateSysUserForm input[name='sysUser.userId']").val(user.userId);
				$("#userLongName").text(user.loginName);
				$("#updateSysUserForm input[name='sysUser.userName']").val(user.userName);
				$("#updateSysUserForm input[name='sysUser.email']").val(user.email);
				$("#updateSysUserForm input[name='sysUser.tel']").val(user.tel);
				$("#updateSysUserForm select[name='sysUser.roleId']").val(user.roleId);
				$("#updateSysUserForm select[name='sysUser.teacherId']").val(user.teacherId);
				$("#updateWin").show();
			}
		},
		error:function(error){
			msgshow("系统繁忙，请稍后再操作！");
		}
	});
}

/**
 * 创建用户
 */
function createSysUser(){
	if($("#confirmPwd").val()!=$("#loginPwd").val()){
		msgshow("两次密码不一致");
		return false;
	}
	var params = ''
	$("#sysUserForm input,#sysUserForm select").each(function(){
		params+=$(this).serialize()+"&";
    });
	$.ajax({
		url:baselocation+'/admin/sysuser/createuser',
		type:'post',
		dataType:'json',
		data:params,
		success:function(result){
			if(result.success==true){
				closedData();
			}
			msgshow(result.message);
		},
		error:function(error){
			msgshow("系统繁忙，请稍后再操作！");
		}
	});
}

function closedData(){
	$("#createWin input:text,#updateWin input:text").val('');
	$("#createWin input:text").val('');
	$("input[name='sysUser.loginName']").val('');
	$("input:password").val('');
	$("select").val(0);
	$("#updateWin,#createWin,#updatePwdWin").hide();
}
/**
 * 赠送课程页面
 */
function selectCousre(userId){
	window.open(baselocation+'/admin/user/courseList/'+userId ,'newwindow', 'toolbar=no,scrollbars=yes,location=no,resizable=no,top=150,left=300,width=923,height=600');
}